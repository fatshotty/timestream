export default {
  "aboutlink": "https://t.me/StreamTime",
  "abouttext": "StreamTime",
  "aspectratio": "16:9",
  "autostart": false,
  "cast": {
    "appid": "00000000"
  },
  "controls": true,
  "defaultBandwidthEstimate": 5000000,
  "displaydescription": false,
  "displaytitle": true,
  "flashplayer": "//ssl.p.jwpcdn.com/player/v/8.12.4/jwplayer.flash.swf",
  "height": 360,
  "key": "g8X91EMRvxZNqaTmiLIyh5sguXNAqUIUD+btWm4PjLv/0ulhGt1U92NNLtESfqXt",
  "logo": {
    "file": "https://assets-jpcust.jwpsrv.com/watermarks/irFZMYBt.png",
    "hide": true,
    "link": "https://t.me/StreamTime",
    "margin": "10",
    "position": "control-bar"
  },
  "mute": false,
  "ph": 1,
  "pid": "ETmbmCY9",
  "playbackRateControls": false,
  "preload": "metadata",
  "repeat": false,
  "skin": {
    "controlbar": {
      "icons": "#5DBCD2",
      "text": "#5DBCD2"
    },
    "menus": {
      "text": "#5DBCD2",
      "textActive": "#5DBCD2"
    },
    "timeslider": {
      "progress": "#5DBCD2"
    },
    "tooltips": {
      "background": "#000000",
      "text": "#5DBCD2"
    }
  },
  "stagevideo": false,
  "stretching": "uniform",
  "width": "100%",
  "captions": {
    "color": '#ffb800',
    "fontSize": 30,
    "backgroundOpacity": 0
  },
  "title": "Test @StreamTime",
  "hlslabels": {
    1096: "480p",
    2096: "720p",
    3596: "1080p"
  }
};
