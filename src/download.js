import Vue from 'vue'

import _download_template_ from "./views/download-button.pug";


Vue.component('Download', {

  template: _download_template_(),

  props: ['link'],

  data: function() {
    return {
      url: ''
    }
  },

  created() {
  },

  mounted() {
    this.$el.addEventListener('pointerdown', (e) => {
      e.stopPropagation();
      this.click();
    })
  },

  computed: {
  },

  methods: {
    click() {
      this.url = this.link;
      setTimeout( () => {
        this.url = '';
      }, 500);
    }
  },

  destroyed() {
  }

});
