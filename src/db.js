    // IndexedDB
    var indexedDB = window.indexedDB || window.webkitIndexedDB || window.mozIndexedDB || window.OIndexedDB || window.msIndexedDB,
        IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.OIDBTransaction || window.msIDBTransaction,
        dbVersion = 1.0;

    // Create/open database

    const DB_NAME = `test_${Date.now()}`;

    let OBJECT = new Promise( (resolve, reject) => {

        var request = indexedDB.open(DB_NAME, dbVersion);

        request.onerror = function (event) {
            console.log("Error creating/accessing IndexedDB database");
            reject();
        };
        request.onsuccess = function (event) {
            console.log("Success creating/accessing IndexedDB database");
            db = request.result;

            db.onerror = function (event) {
                console.log("Error creating/accessing IndexedDB database");
            };

            resolve();
        };
        request.onupgradeneeded = function (event) {
            createObjectStore(event.target.result);
        };
    });


    let db,
        createObjectStore = function (dataBase) {
            // Create an objectStore
            console.log("Creating objectStore")
            dataBase.createObjectStore(DB_NAME);
        },

        getImageFile = function (url) {
          // Create XHR
          return new Promise( (resolve, reject) => {

            var xhr = new XMLHttpRequest(),
                blob;

            xhr.open("GET", url, true);
            // Set the responseType to blob
            xhr.responseType = "arraybuffer";

            xhr.onload = function () {
                if (xhr.status === 200) {
                    console.log("Image retrieved", !!xhr.response);

                    // Blob as response

                    // Put the received blob into IndexedDB
                    if ( xhr.response )
                      resolve(xhr.response);
                    else
                      reject('response is null')
                }
            };
            // Send XHR
            xhr.send();
          });
        },

        putElephantInDb = function (ab, key) {

            console.log("Putting elephants in IndexedDB", key, ab);

            // Open a transaction to the database
            var transaction = db.transaction([DB_NAME], 'readwrite');

            // Put the blob into the dabase
            var put = transaction.objectStore(DB_NAME).put( new Blob([ab], {type: 'application/octet-stream'}), `image${key}`);

            // Retrieve the file that was just stored
            return Promise.resolve();
        },

        getData = function(key) {
          return new Promise( (resolve, reject) => {
            var transaction = db.transaction([DB_NAME], 'readonly');
            console.log(`getting image${key}`);
            let req = transaction.objectStore(DB_NAME).get(`image${key}`)
            // req.error = function() {
            //   console.log('error extracting', key);
            //   reject();
            // };
            req.onsuccess = function (event) {
              var imgFile = event.target.result;
              console.log("Got elephant!" + imgFile);

              // Get window.URL object
              var URL = window.URL || window.webkitURL;

              // Create and revoke ObjectURL
              var imgURL = URL.createObjectURL(imgFile);

              // Set img src to ObjectURL
              // var imgElephant = document.getElementById("elephant");
              // imgElephant.setAttribute("src", imgURL);
              let a = document.createElement('a');
              a.href = imgURL;
              a.setAttribute('download', `file_${key}`);
              a.innerHTML = `download ${key}!`;
              document.body.appendChild(a);

              resolve( imgFile );
            };
          });
        },

        getAll = function(keys) {
            // let ps = keys.map( (k) => {
            //   return getData(k)
            // });
            // return Promise.all(ps);
          return new Promise( (resolve, reject) => {
            var transaction = db.transaction([DB_NAME], 'readonly');
            let req = transaction.objectStore(DB_NAME).getAll();
            console.log('getting all data');
            req.onsuccess = function (event) {
                console.log("Got elephants", event);
                resolve( event.target.result );
            };
            req.onerror = function() {
                reject();
            }
          });
        }



    // request.onsuccess = function (event) {
    //     console.log("Success creating/accessing IndexedDB database");
    //     db = request.result;

    //     db.onerror = function (event) {
    //         console.log("Error creating/accessing IndexedDB database");
    //     };

    //     // Interim solution for Google Chrome to create an objectStore. Will be deprecated
    //     if (db.setVersion) {
    //         if (db.version != dbVersion) {
    //             var setVersion = db.setVersion(dbVersion);
    //             setVersion.onsuccess = function () {
    //                 createObjectStore(db);
    //                 getImageFile();
    //             };
    //         }
    //         else {
    //             getImageFile();
    //         }
    //     }
    //     else {
    //         getImageFile();
    //     }
    // }

    // // For future use. Currently only in latest Firefox versions
    // request.onupgradeneeded = function (event) {
    //     createObjectStore(event.target.result);
    // };

    export default {
        loadUrl(url) {
          return OBJECT.then( () => {
            return getImageFile(url);
          })
        },
        save: putElephantInDb,
        getData,
        getAll
    }
