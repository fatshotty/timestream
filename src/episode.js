import Vue from 'vue'

import './download'

import _episode_template_ from "./views/episode-details.pug";


Vue.component('Episode', {

  template: _episode_template_(),

  props: ['episode', 'index', 'showdownload'],

  data: function() {
    return {
    }
  },

  created() {
  },

  mounted() {
    this.$el.addEventListener('pointerdown', (e) => {
      e.stopPropagation();
      this.clickEpisode();
    })
  },

  computed: {
    downloadLink() {
      return this.episode.down;
    },
    episodeTitle() {
      return this.episode.title || `Episodio ${this.episode.num < 10 ? '0' + this.episode.num : this.episode.num}` ;
    },
    episodePlot() {
      let text = this.episode.plot.split(' ');
      let str = '';
      let result = [];
      let add_points = false;
      for ( let word of text ) {
        str += ` ${word}`;
        if ( str.length <= 100 ) {
          result.push( word );
        } else {
          add_points = true;
          break;
        }
      }
      let descr = result.join(' ').trim();
      return `${descr}${add_points ? ' ...' : ''}`;
    }
  },

  methods: {
    clickEpisode() {
      this.$parent.$emit('select-episode', this.episode, this.index);
    }
  },

  destroyed() {
  }

});
