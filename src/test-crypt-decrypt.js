const AES = require('./crypt/AES');
const FS = require('fs');

const aes = new AES(undefined, 256);
const key = aes.randomKeyGen(32, true);
let origKey = FS.readFileSync('./v/test.key');

var path = './v/file_0';

aes.p_encryptFile(path, origKey);

aes.p_decryptFile( `${path}.enc`, origKey)
