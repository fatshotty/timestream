import Vue from 'vue'

import _playlist_template_ from "./views/playlist-component.pug";


Vue.component('Playlist', {
  template: _playlist_template_(),

  props: ['episodes', 'current'],

  data: function() {
    return {};
  },
  created() {
    this.$on('select-episode', (episode, index) => {
      this.selectEpisode(index);
    })
  },
  mounted() {

  },
  computed: {

  },
  watch: {
    current: {
      immediate: true,
      handler: function(new_value) {
        // TODO: scroll list
        if ( this.episodes.length > 1 && this.$el ) {
          this.scrollTo(new_value);
        }
      }
    }
  },
  methods: {
    scrollTo(index) {
      // TODO: scroll to index
      let scrollContainer = this.$el.querySelector('.episodes-container');
      let allepisodes = Array.prototype.slice.call(scrollContainer.querySelectorAll('.episode-item'), 0);
      let currentepisode_el = allepisodes[index];
      if (currentepisode_el) {
        scrollContainer.scrollTop = currentepisode_el.offsetTop;
        scrollContainer.scrollLeft = currentepisode_el.offsetLeft;
      } else {
        console.warn('no episode at index', index)
      }
    },
    selectEpisode(index) {
      this.$root.$emit('next-episode', index);
    }
  },
  destroyed() {

  }
});

