import Vue from 'vue'
import _wrapper_template_ from "./views/wrapper-component.pug";

Vue.component('Wrapper', {
  template: _wrapper_template_(),
  props: ['playlist', 'autoplay', 'beforenextepthreshold'],
  data: function() {
    return {
      currentIndex: -1,
      nextEpisodeOverlay: true,
      autostart: false
    }
  },
  created() {

    this.$on('select-episode', (episode, index) => {
      this.nextEpisodeAutoPlay();
    })

    this.$root.$on('next-episode', (index) => {
      this.onNextEpisode(index);
    });

    this.$root.$on('episode-ended', () => { this.nextEpisodeAutoPlay() } );
    this.$root.$on('near-end-episode',() => { this.onNearEndEpisode() } );

    this.$root.$on('on-playing', () => {
      this.autostart = true;
    });

    this.autostart = !!this.autoplay;


  },
  mounted() {
    this.nextEpisode()
  },
  computed: {
    currentEpisode() {
      return this.playlist[ this.currentIndex ];
    },
    allEpisodes() {
      return this.playlist;
    },
    currentEpisodeIndex() {
      return this.currentIndex;
    },
    nextEpisodeInOverlay() {
      let nextIndex = this.currentIndex + 1;
      if ( nextIndex === this.currentIndex ) {
        return null;
      }
      if ( nextIndex >= this.playlist.length ) {
        return null;
      }
      return this.playlist[ nextIndex ];
    }
  },
  watch: {
  },

  methods: {

    onNearEndEpisode() {
      this.nextEpisodeOverlay = true;
    },

    onNextEpisode(index) {
      if ( index === this.currentIndex ) {
        return;
      }
      if ( index >= this.playlist.length ) {
        return;
      }

      this.nextEpisodeOverlay = false;
      this.currentIndex = index;
    },

    nextEpisode() {
      let nextIndex = this.currentIndex + 1;
      this.onNextEpisode(nextIndex);
    },

    nextEpisodeAutoPlay() {
      this.autostart = true;
      this.nextEpisode();
    }

  },

  destroyed() {
  }
})
