import Vue from 'vue'
// import './player-runtime';
import PlayerConfig from './player-config'

jwplayer.defaults = PlayerConfig;

import _player_template_ from "./views/player-component.pug";

const CHUNK = 20 * 1024 * 1024; // 10M

Vue.component('Player', {
  template: _player_template_(),
  props: ['episode', 'threshold', 'autoplay'],
  data: function() {
    return {
      nextEpisodeTriggered: false,
      hasMetadata: false,
      skipIntro: 0
    }
  },
  created() {
    this._threshold = parseInt(this.threshold, 10) || 0;
  },
  mounted() {

    this.player = jwplayer( this.$el.querySelector('#player-inner-container') )
    // .setup({
    //   image: "http://Stsh.ml/Img-FinalDestination5-2011",
    //   tracks:[{"file":"LINK SUBS","label":"ITA FORCED","kind":"captions","default":"true"}],
    //   captions: {color: '#ffb800',fontSize: 30,backgroundOpacity: 0},
    //   file: "https://assets.ctfassets.net/nx6rkmusdn8d/4Fsx4L9SzQmdWVyq5xC2yF/7c21790ea761f6ebc8881111a1033202/test_2.m3u8",
    //   title: "Test @StreamTime",
    //   hlslabels: {
    //     1096: "480p",
    //     2096: "720p",
    //     3596: "1080p"
    //   }
    // });


    // this.videoEl = this.$el.querySelector('video');
    // this.player = new Plyr( this.videoEl );

  },
  computed: {
    episodeNumber() {
      return this.currentMedia.num
    },
    currentMedia() {
      return this.episode
    },
    plot_details() {
      return this.currentMedia.plot;
    }
  },
  watch: {
    currentMedia: {
      immediate: false,
      handler: function(new_value){
        if ( ! new_value ) {
          this.stop();
          return;
        }
        this.setNewSource(new_value);
      }
    }
  },
  methods: {

    seekSkipIntro() {
      this.player.seek( this.currentMedia.skipIntro );
    },

    onPlaying() {
      this.skipIntro = this.skipIntro > 0 ? this.skipIntro :  (!!this.currentMedia.skipIntro ? 100 : 0);
      this.$root.$emit('on-playing');
    },

    currentMediaTitle() {
      return this.currentMedia.title || `Episodio ${this.episodeNumber < 10 ? '0' + this.episodeNumber : this.episodeNumber}`
    },

    onLoadedMetaData() {
      if ( this.player.getDuration() > 0 ) {
        this.hasMetadata = true;
        // console.info('metadata has duration', this.player.duration);
      }
    },

    onTimeupdate() {
      // TODO: check this.player.duration and trigger next-episode
      // this.player.currentTime
      if ( this.hasMetadata ) {
        if ( this._threshold > 0 && ! this.nextEpisodeTriggered) {
          if ( this.player.getDuration() - this.player.getPosition() <= this._threshold ) {
            this.nextEpisodeTriggered = true;
            this.$root.$emit('near-end-episode');
          }
        }


        if ( this.currentMedia.skipIntro ) {
          let skipIntroValue = this.currentMedia.skipIntro - this.player.getPosition();

          if ( skipIntroValue > 0 ) {
            // show 'skip-intro'
            console.log('skip intro', true)
            this.skipIntro = 100 - (100 * skipIntroValue / this.currentMedia.skipIntro)

          } else {
            // hide 'skip-intro'
            console.log('skip intro', false);
            this.skipIntro = 0
          }
        }

      }
    },

    onEnded() {
      // TODO: trigger ended event
      this.$root.$emit('episode-ended');
    },


    setNewSource(episode) {
      // this.nextEpisodeTriggered = false;
      // this.hasMetadata = false;

      let ep = {
        type: 'video',
      //   sources: [{
      //     src: episode.src,
      //     type: 'video/mp4'
      //   }]
      };

      if ( episode.poster || episode.fanart ) {
         ep.image = episode.poster || episode.fanart;
      }

      if ( episode.tracks || episode.subtitle ) {
        ep.tracks = [ Object.assign({}, episode.tracks || episode.subtitle) ];
      }

      ep.title = episode.title;
      ep.file = episode.src;
      ep.description = ep.plot;


      this.player.setup( ep );
      this.player.on('time', () => {
        this.onTimeupdate();
      });

      this.player.on('complete', () => {
        this.onEnded();
      });

      this.player.on('meta', () => {
        this.onLoadedMetaData();
      });

      this.player.on('play', () => {
        this.onPlaying();
      });

    },


    play() {
      this.player.play();
    },

    pause() {
      this.player.pause();
    },

    stop() {
      // TODO: stop current playing player
      this.skipIntro = 0
    }

  },

  destroyed() {
    this.stop();
    // this.player.destroy();
  }
})
