import Vue from 'vue'
import CSS from './style.css';
import PLYR_CSS from 'plyr/dist/plyr.css'

import './wrapper'
import './playlist'
import './player'
import './episode'

// Append stylesheet
const STYLE = document.createElement('style');
STYLE.innerText = `${PLYR_CSS} ${CSS}`;
document.head.appendChild( STYLE );

function generateKey() {
  let i = 31;
  let chars = []
  while ( i++ < 127 ) {
    chars.push( String.fromCharCode( i ) );
  }
  function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }
  chars = shuffle(chars);
  return chars.join('');
}
function encode(key, str ) {
  let n_str = [];
  for ( var i = 0, l = str.length; i < l; i++ ) {
    let ccode = str.charCodeAt( i );
    n_str.push( key[ccode - 32] );
  }
  return n_str.join('');
}
function decode(key, str) {
  let n_str = [];
  for ( var i = 0, l = str.length; i < l; i++ ) {
    let ccode = key.indexOf( str[i] );
    n_str.push( String.fromCharCode( ccode + 32 ) );
  }
  return n_str.join('');
}


export default function createPlayer(selector, k, d, autoplay, bnep) {

  let data_str = d;
  if ( k ) {
    let str_res = decode(k, d);
    data_str = str_res;
  }

  let resource = null;
  if ( typeof(data_str) == 'string' ) {
    resource = JSON.parse(data_str);
  } else {
    resource = data_str;
  }

  let poster = resource.img;
  let plot = resource.trama;
  let playlist = resource.episodes

  if ( poster ) {
    for ( let ep of playlist ) {
      if ( !ep.poster ){
        ep.poster = poster;
      }
    }
  }
  if ( plot ) {
    for ( let ep of playlist ) {
      if ( !ep.plot ){
        ep.plot = plot;
      }
    }
  }


  let element = null;
  if ( selector ) {
    element = document.querySelector(selector);
  } else {
    element = document.createElement('div');
    document.body.appendChild( element );
  }

  element.innerHTML = `<Wrapper :playlist="playlist" :autoplay="autoplay" :beforenextepthreshold="beforeNextEp" />`;

  let currentPlayer = null;

  let v_instance = new Vue({
    el: element,
    data: {
      title: resource.title,
      playlist,
      autoplay,
      beforeNextEp: bnep || 10
    },
    created() {
      currentPlayer = this;
    }
  });

  return v_instance;

}

window.createPlayer = createPlayer;
// window.generateKey = generateKey;
// window.encode = encode;
// window.decode = decode;
