import DB from './db'
import Decrypter from './crypt/decrypter'

let decrypt = new Decrypter({enableSoftwareAES: true}, {removePKCS7Padding: true});

// let downloads = {
//   key: 'http://path/to/key',
//   iv: 'http://path/to/IV',
//   links: [
//     'http://path/to/file'
//   ]
// };

  let downloads = {
    key: '/v/test.key',
    // iv: '/v/test.iv',
    links: [
      '/v/xaa.enc'
    ]
  };

function save(data, mediatype, filename) {
  data = Array.isArray(data) ? data : [data];
  console.log('saving file', data.length, filename);
  let properties = {type: mediatype || 'application/octet-stream'}; // Specify the file's mime-type.
  let file = new Blob(data, properties);
  let url = URL.createObjectURL(file);
  let a = document.createElement('a');
  a.href = url;
  a.setAttribute('download', filename || 'video_downloaded.mp4');
  a.innerHTML = '- download total -';
  document.body.appendChild(a);
  console.info('finish');
}

function getFile(url) {
  return new Promise( (resolve, reject) => {
    var oReq = new XMLHttpRequest();

    oReq.open("GET", `${url}?ts=${Date.now()}`, true);
    oReq.responseType = "arraybuffer";
    oReq.onload = function(e) {
      resolve(oReq.response);
    };
    oReq.send();
  });
}


function extractIV(buff) {
  return buff.slice(0, 16)
}


function decryptAll(buffs, crypt, KEY, IV) {
  let totalBuff = [];
  let i = 0;
  function exc(cb) {
    if ( buffs.length <= 0 ) {
      return cb();
    }
    let buff = buffs.shift();
    decrypt[crypt ? 'encrypt' : 'decrypt'](buff, KEY, IV, (resBuff) => {
      if ( resBuff ) {
        totalBuff.push( resBuff );
        exc(cb);
      }
    });
  }

  return new Promise( (resolve,reject) =>{
    exc( () => {
      let b = new Blob(totalBuff, {type: 'application/octet-stream'});
      b.arrayBuffer().then( resolve )
    })
  })

}

function decryptChunk(buff, crypt, chunk, KEY, IV, totalBuff) {
  chunk = chunk || buff.byteLength;
  while( buff.byteLength > 0 ) {
    let chunkBuff = buff.slice(0, chunk);
    buff = buff.slice( chunk );
    totalBuff.push( chunkBuff );
  }

  return decryptAll( totalBuff, crypt, KEY, IV );

  // return new Promise( (resolve, reject) => {

  //   let chunkBuff = buff.slice(0, chunk);
  //   if ( (new Uint8Array(chunkBuff)).length <= 0 ) {
  //     let b = new Blob(totalBuff, {type: 'application/octect-stream'});
  //     return b.arrayBuffer(resolve);
  //   }
  //   buff = buff.slice( chunk );
  //   decrypt[crypt ? 'encrypt' : 'decrypt'](chunkBuff, KEY, IV, (resBuff) => {
  //     totalBuff.push( resBuff );
  //     resolve( decryptChunk( buff, crypt, chunk, KEY, IV, totalBuff ) )
  //   })
  // })
}


function execute(crypt, json) {

  Promise.all([ getFile(downloads.key), downloads.iv ? getFile(downloads.iv) : Promise.resolve(undefined) ]).then( (res) => {
    let KEY = res[0];
    let IV = downloads.iv ? res[1] : null;

    downloads = json || downloads;

    let links = downloads.links.slice(), i = 0;
    function down() {
      let link = links.shift();
      if ( !link ) {
        console.log('no more link')
        return Promise.resolve(undefined);
      } else {
        return getFile(link).then( (buff) => {
          IV = IV || (crypt && window.crypto.getRandomValues(new Uint8Array(16)).buffer);
          let filesize = new Int32Array([buff.byteLength, 0]).buffer;
          if (! crypt ) {
            // filesize = buff.slice(0, 8);
            // buff = buff.slice(8);
            if ( ! IV ) {
              // KEY = buff.slice(0, 16);
              // buff = buff.slice(16);
              IV = buff.slice(0, 16);
              buff = buff.slice(16);
            }
          }

          return decryptChunk(buff, crypt, false, KEY, IV, []).then( (ab) => {
            if ( crypt ) {
              ab = [IV, ab];
            }
            save(ab, 'video/mp4', 'test_video');
            // return DB.save(ab, i++);
          })

        }).then( down );
      }
    }

    down().then( () => {
      return;
      console.log('all file downloaded');


      let i_down = -1, bs = [];
      function gettingData() {
        if ( ++i_down >= downloads.links.length ) {
          return Promise.resolve(undefined);
        }
        return DB.getData( i_down ).then( (b) => {
          bs.push( b );
        }).then( gettingData );
      }

      gettingData().then( () => {
        console.log('got all buffers')
        save(bs, 'video/mp4', 'test_video');
      });

    })

  });

}

window.Crypt = (json) => {
  execute(true, json);
}
window.Decrypt = (json) => {
  execute(false, json);
}
