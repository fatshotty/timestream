import CSS from './style-player.css';

import Config from './player-config';

const STYLE = document.createElement('style');
STYLE.innerText = `${CSS}`;
document.head.appendChild( STYLE );

const script = document.createElement('script');
script.src = "https://cdn.jwplayer.com/libraries/ETmbmCY9.js";
document.body.appendChild( script );

let ScriptLoaded = new Promise( (resolve, reject) => {

  script.onload = resolve;

  script.onerror = reject

});

let mainContainer = createElement(`<div class="player-outer-container"><div id="player-element"></div></div>`);

function dk(key, str) {
  let n_str = [];
  for ( var i = 0, l = str.length; i < l; i++ ) {
    let ccode = key.indexOf( str[i] );
    n_str.push( String.fromCharCode( ccode + 32 ) );
  }
  return n_str.join('');
}

function initPlayerK(parentEl, data) {
  let k = data.substring(0, 95);
  let dk = data.substring(96);
  return initPlayer(parentEl, dk, k);
}
function initPlayer(parentEl, data, k) {

  (parentEl || document.body).appendChild(mainContainer);

  ScriptLoaded.then( () => {

    window.jwplayer.defaults = Config;

    let PlayerEngine = window.jwplayer( mainContainer.querySelector('#player-element') );

    data = buildJsonData(data, k);

    let Player = PlayerEngine.setup( data );

    const buttonId = 'download-video-button';
    const iconPath = 'https://www.jwplayer.com/developers/web-player-demos/add-download-button/assets/download.svg';
    const tooltipText = 'Download';

    Player.addButton(iconPath, tooltipText, () => {
      // let currentItem = Player.getPlaylistItem();
      // let a = createElement(`<a href="${currentItem.sources[0].file}" download="${currentItem.title}" target="_blaknk" style="display: none">download</a>`);
      // document.body.appendChild(a);
      // a.click();
      // a.remove();
      alert('download started');
    }, buttonId);

    Player.on('playlistItem', ({index, item}) => {
      destroySkipIntro();
    });

    Player.on('time', () => {
      let playlistIndex = Player.getPlaylistIndex();
      let playlistItem = Player.getPlaylistItem( playlistIndex );

      if ( playlistItem.skipIntro ) {
        let skipintroObj = ((value)=>{
          let skipIn, skipOut;
          if ( typeof value == 'object' ) {
            skipIn = value.show || 0;
            skipOut = value.seek;
          } else {
            skipIn = 0;
            skipOut = value;
          }
          return {skipIn, skipOut};
        })(playlistItem.skipIntro);

        let currentPos = Player.getPosition();

        if ( currentPos >= skipintroObj.skipIn ) {
          let skipIntroCountDown = skipintroObj.skipOut - currentPos;
          if ( skipIntroCountDown > 0 ) {
            // show Skip Intro Button
            createOrUpdateSkipIntro(skipIntroCountDown, skipintroObj.skipIn, skipintroObj.skipOut, Player);
            return;
          }
        }
      }
      destroySkipIntro();
    })
  })

}


function createElement(html) {
  const tmpDiv = document.createElement('div');
  tmpDiv.innerHTML = html;
  let first = tmpDiv.firstChild;
  first.remove();
  return first;
}

const SkipIntro = createElement(
  `<div class="jw-nextup-container jw-reset jw-nextup-sticky jw-nextup-container-visible">
    <div class="jw-nextup jw-background-color jw-reset jw-nextup-thumbnail-visible jw-skip-intro">
      <div class="jw-nextup-tooltip jw-reset">
        <div class="jw-nextup-body jw-reset">
          <div class="jw-nextup-duration jw-reset jw-background-time"></div>
          <button class="jw-nextup-title jw-reset-text">SALTA INTRO</button>
        </div>
      </div>
    </div>
  </div>`
);

function createOrUpdateSkipIntro(counter, skipIn, skipOut, Player) {
  if ( ! SkipIntro.parentNode ) {
    let p = mainContainer.querySelector('.jw-controls.jw-reset');
    p && p.appendChild(SkipIntro);
    let btn = SkipIntro.querySelector('button');
    if ( btn && btn.parentNode) {
      btn.parentNode.onclick = function() {
        Player.seek( skipOut );
      };
    }
  }
  let bg = SkipIntro.querySelector('.jw-background-time');
  if ( bg ) {
    bg.style.left =  `${100 - (100 * counter / (skipOut - skipIn))}%`;
  }
}

function destroySkipIntro() {
  SkipIntro.remove();
}


/** Utils */

function buildJsonData(data, k) {
  if ( k && typeof data === 'string') {
    let dataobj = dk(k, data);
    return JSON.parse(dataobj);
  }
  return data;
}

window.cP = initPlayer;
window.cPk = initPlayerK;

// console.info( window.cP, window.cPk )

export default {}
