
module.exports = {

  entry: {
    "stplayer": "./src/main-player.js",
    "test": "./src/test.js"
  },

  mode: "production",
  watch: false,

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
      },
      {
        test: /\.css$/i,
        use: ['css-loader'],
      },
      {
        test: /\.pug$/,
        use: ['pug-loader']
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js' // 'vue/dist/vue.common.js' for webpack 1
    }
  },
  output: {
    path: `${__dirname}/`,
    filename: "[name].js",
    pathinfo: true,
    sourceMapFilename: "[file].js.map"
  }
};
